##Data Availability Statements

**Status:** PUBLIC REVIEW  
**Version:** 1.1  
**Date:** 5 Jan 2018  
**Commenting Period:** 30th April to 31st May 2018
#Context 
``
<back>
``
``<ref-list>
``

#Description
There are three types of data that may be associated with an article: 

* **Generated data:** Included or referenced data that were generated for the study  

* **Analysed data:** Referenced data that were analysed for the study but were not generated for the study

* **Non-analysed data:** Referenced data that were neither generated nor analysed for the study
 
Full, structured citations should be provided for any referenced data; i.e., externally archived generated data, analysed data, and non-analysed data.  
Recommended practice is to indicate the location(s) of any data that were generated or analysed for the study in a Data Availability statement (DAS). We recommend excluding non-analysed data from the DAS, and instead, only including it in the main reference list of the article. For any data that are not provided within the paper (i.e., as a table or a supplementary file/source data file), the DAS should provide access to the external data locations, either as direct links, or as links to the relevant reference included in the article’s reference list(s). If any generated or analysed data is not publicly available, the DAS should provide reasons for the omission. 

**Additional reading**

* Force11 Joint Declaration of Data Citation Principles.
Cousijn et al, 2017. A Data Citation Roadmap for Scientific Publishers. BioRxIV, DOI:  https://doi.org/10.1101/100784.  
* Crossref : depositing data citations. 

#Recommendations
1. ``<back>`` Contain the DAS in a ``<sec>`` element within ``<back>``.  
[[Validator result: ERROR: ``<sec @sec-type="data-availability">`` found outside ``<back>`` ]]

2. @sec-type="data-availability" should be used on this ``<sec>``.  
[[Validator result: ERROR: ``<sec @sec-type="">`` with following values found anywhere in XML document: if encounter any @sec-type labelled “Data availability”, “Data Availability”, “Data-Availability”, “data availability statement”, “Data availability statement”, “Data Availability Statement”, “data-availability-statement” “Data-availability-statement”, “Data-Availability-Statement”, “Data_Availability”, “data_availability-statement”, “Data_availability-statement”, “Data_Availability-Statement”, "data_availability" ]]

3. Use a ``<title>`` element to contain the title (Data Availability), and contain the substance of the text within ``<p>``.  
[[Validator result: WARNING: ``<title>`` not found, or content not match “Data Availability”]]

4. ``<ref-list>`` References for the data. There are four options for capturing references to data:  
 * Contain the references within the main <ref-list> for the article (as recommended by the Force11 Publishers Early Adopters Expert Group)
 * Contain references to data within a <ref-list> directly in the ``<sec sec-type="data-availability"> ``element
 * insert the data citations solely as ``<element-citation>`` or ``<mixed-citation>`` elements directly in the ``<sec sec-type="data-availability">``
 * Contain the data references within a sub-level ``<ref-list>`` at the end of the article. 

5. ``<element-citation>`` or ``<mixed-citation>``, @publication-type="data". Use this attribute on all ``<element-citation>`` or ``<mixed-citation>`` elements that contain references to data.  
[[Validator results: see validator rules for JATS4R recommendation on data citations]]
6. @specific-use. Publishers who use the Relation Type method for Crossref deposits should  use @specific-use on ``<element-citation>`` or ``<mixed-citation>`` to contain appropriate CrossRef  inter_work_relation/@relationship-type values, as follows:


 Type of data reference| inter_work_relation/@relationship-type
------------- | -------------
Generated data that is externally archived | “isSupplementedBy”
Analyzed data  | “references”
Non-analyzed data  | “references”
 

**Provenance:** JATS4R sub-group.  
JATS4R subgroup members (listed alphabetically):  
Ton Bos (Elsevier.com). 
Paul Donohoe (Springernature.com) 
Melissa Harrison (Chair, eLife)
Christina Von Raesfeld (PLOS)
Kelly McDougall (MIT Press)

**History:**
Working: Commenced with a call on Thursday 12th January 2017. 
Five calls were held, last one Thursday 20th April. 
Committee review: NA. 
Pending Decision: 21st April 2017. The group proposed to the JATS Standing Committee that a new element be created: <data-availability>, to be contained within <back>, with the same content model as <sec>. This request was denied in November 2017. 
Working: November 2017. Last call held January 5th 2018. 
Committee review: XXXXXX
Public Review: XXXXXX

##Examples

#Example 1:  Data files provided in the paper 

```xml
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p> All data are provided within the paper and its supporting information.</p>
</sec>
<ref-list>
<title>References</title>
...
</ref-list>
</back>
```

Example 2: Data files provided in the paper – includes links to the specific internal data files

```xml 
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p> All data are provided in <xref ref-type="table" rid="table1">
Table 1</xref> and Datasets <xref ref-type="supplementary-material" rid="data1">S1</xref>  and <xref ref-type="supplementary-material" rid="data2">S2</xref>.</p>
</sec>
<ref-list>
<title>References</title>
...
</ref-list>
</back>
Example 3: Data externally archived – links to full citations in main References (using Vancouver style - numbered references)
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p>The data analysis file and all annotator data files are available in the Figshare repository, <ext-link ext-link-type="uri" xlink:href="https://doi.org/10.6084/m9.figshare.1285515">https://doi.org/10.6084/m9.figshare.1285515</ext-link> [<xref ref-type="bibr" rid="pone.0167292.ref032">32</xref>]. The measured and simulated Euler angles, and the simulation codes are available from the Dryad database, <ext-link ext-link-type="uri" xlink:href="https://doi.org/10.5061/dryad.cv323">https://doi.org/10.5061/dryad.cv323</ext-link> [<xref ref-type="bibr" rid="pone.0167292.ref033">33</xref>]. Microarray data are deposited in the Gene Expression Omnibus under accession number <ext-link ext-link-type="uri" xlink:href="https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE70542>GSE70542</ext-link> [<xref ref-type="bibr" rid="pone.0167292.ref034">34</xref>].</p>
</sec>
<ref-list>
<title>References</title>
[Insert references here]
</ref-list>
</back>
```

Example 4: Data externally archived – full citations included as a ref list within the DAS

```xml
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p>The following datasets were generated or analyzed for this study:</p>
<ref-list>
<ref id="pone.0167830.data001">
<label>D1</label>
<element-citation publication-type="data" 
    use-case=”generated-data”
    specific-use=”isSupplementedBy”>
<name>
<surname>Read</surname> 
<given-names>K</given-names>
</name>
<data-title>Sizing the Problem of Improving Discovery and Access to NIH-funded Data: A Preliminary Study (Datasets)</data-title>
<source>Figshare</source>
<year>2015</year>
<pub-id pub-id-type="doi" assigning-authority="figshare" xlink:href=
"https://doi.org/10.6084/m9.figshare.1285515">https://doi.org/10.6084/m9.figshare.1285515</pub-id>
</element-citation>
</ref>
<ref id="pone.0167830.data002">
<label>D2</label>
<element-citation publication-type="data" 
   use-case=”analyzed”
   specific-use=”references”>
<name>
<surname>Kok</surname> 
<given-names>K</given-names>
</name>
<name>
<surname>Ay</surname> 
<given-names>A</given-names>
</name>
<name>
<surname>Li</surname> 
<given-names>L</given-names>
</name> 
<data-title>Genome-wide errant targeting by Hairy</data-title>
<source>Dryad Digital Repository</source>
<year>2015</year>
<pub-id pub-id-type="doi" assigning-authority="dryad" xlink:href=
"https://doi.org/10.5061/dryad.cv323">https://doi.org/10.5061/dryad.cv323</pub-id>
</element-citation>
</ref>
<ref id="pone.0167830.data003">
<label>D3</label>
<element-citation publication-type="data" s
   use-case=”analyzed”
  specific-use=”references”>
<name>
<surname>Hoang</surname> 
<given-names>C</given-names>
</name>
<name>
<surname>Swift</surname> 
<given-names>GH</given-names>
</name> 
<name>
<surname>Azevedo-Pouly</surname> 
<given-names>A</given-names>
</name>
<name>
<surname>MacDonald</surname> 
<given-names>RJ</given-names>
</name>
<data-title>Effects on the transcriptome of adult mouse pancreas (principally acinar cells) by the inactivation of the Ptf1a gene in vivo</data-title>
<source>NCBI Gene Expression Omnibus</source>
<year>2015</year>
<pub-id pub-id-type="accession" assigning-authority="NCBI" xlink:href="https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE70542">GSE70542</pub-id>
</element-citation>
</ref>
</ref-list>
</sec>
<ref-list>
<title>References</title>
...
</ref-list>
</back>
```

```
xml
Example 5: Data externally archived – full citations included in the DAS, but not within a ref list 
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p>The data analysis file and all annotator data files are available in the Figshare repository:
<element-citation publication-type="data"
   use-case=”generated-by”
   specific-use="isSupplementedBy">
<name>
<surname>Read</surname>
<given-names>K</given-names>
</name>
<data-title>Sizing the Problem of Improving Discovery and Access to NIH-funded Data: A Preliminary Study (Datasets)</data-title>
<source>Figshare</source>
<year>2015</year>
<pub-id pub-id-type="doi" assigning-authority="figshare" xlink:href=
"https://doi.org/10.6084/m9.figshare.1285515">https://doi.org/10.6084/m9.figshare.1285515</pub-id>
</element-citation></p>
<p>The measured and simulated Euler angles, and the simulation codes are available from the Dryad database:
<element-citation publication-type="data" 
  use-case=”analyzed”
  specific-use="references">
<name>
<surname>Kok</surname>
<given-names>K</given-names>
</name>
<name>
<surname>Ay</surname>
<given-names>A</given-names>
</name>
<name>
<surname>Li</surname>
<given-names>L</given-names>
</name>
<data-title>Genome-wide errant targeting by Hairy</data-title>
<source>Dryad Digital Repository</source>
<year>2015</year>
<pub-id pub-id-type="doi" assigning-authority="dryad" xlink:href=
"https://doi.org/10.5061/dryad.cv323">https://doi.org/10.5061/dryad.cv323</pub-id>
</element-citation>
</p>
<p> Microarray data are deposited in the Gene Expression Omnibus under accession number GSE70542:
<element-citation publication-type="data" 
  use-case=”analyzed”
  specific-use="references">
<name>
<surname>Hoang</surname>
<given-names>C</given-names>
</name>
<name>
<surname>Swift</surname>
<given-names>GH</given-names>
</name>
<name>
<surname>Azevedo-Pouly</surname>
<given-names>A</given-names>
</name>
<name>
<surname>MacDonald</surname>
<given-names>RJ</given-names>
</name>
<data-title>Effects on the transcriptome of adult mouse pancreas (principally acinar cells) by the inactivation of the Ptf1a gene in vivo</data-title>
<source>NCBI Gene Expression Omnibus</source>
<year>2015</year>
<pub-id pub-id-type="accession" assigning-authority="NCBI" xlink:href="https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE70542">GSE70542</pub-id>
</element-citation>
</p>
</sec>
<ref-list>
<title>References</title>
….
</ref-list>
</back>
```

Example 6: Data externally archived – links to full citations in a separate section for Data References (ref links have an alternative labeling scheme)

```xml
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p>The data analysis file and all annotator data files are available in the Figshare repository, <ext-link ext-link-type="uri" xlink:href="https://doi.org/10.6084/m9.figshare.1285515">https://doi.org/10.6084/m9.figshare.1285515</ext-link> [<xref ref-type="bibr" rid="pone.0167292.data001">D1</xref>]. The measured and simulated Euler angles, and the simulation codes are available from the Dryad database, <ext-link ext-link-type="uri" xlink:href="https://doi.org/10.5061/dryad.cv323">https://doi.org/10.5061/dryad.cv323</ext-link> [<xref ref-type="bibr" rid="pone.0167292.data002">D2</xref>]. Microarray data are deposited in the Gene Expression Omnibus under accession number <ext-link ext-link-type="uri" xlink:href="https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE70542>GSE70542</ext-link> [<xref ref-type="bibr" rid="pone.0167292.data003">D3</xref>].</p>
</sec>
<ref-list>
<title>References</title>
….
</ref-list>
<ref-list>
<title>Data References</title>
[Insert references here]
</ref-list>
</back>
```

Example 7 Data cannot be made publicly available.

```xml
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p> Ethical restrictions according to the Japanese Ethical Guidelines for Human Genome/Gene Analysis Research (http://www.lifescience.mext.go.jp/files/pdf/n796_00.pdf, page 33) prevent public sharing of individual genotype data. All summarized data are available upon request. Data requests may be sent to the UMIN IRB (irb@xxxxxxxx.jp)..</p>
</sec>
<ref-list>
<title>References</title>
...
</ref-list>
</back>
```

Example 8: No data was generated [This seems unlikely, but it's possible, e.g., for theoretical works]

```xml
<back>
. . .
<sec sec-type="data-availability">
<title>Data Availability</title>
<p> During the course of this research no data was analysed, reused or generated.</p>
</sec>
<ref-list>
<title>References</title>
...
</ref-list>
</back>
```


